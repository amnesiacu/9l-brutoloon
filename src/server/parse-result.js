const path = require('path')
const fs = require('fs')
const { formatMapping } = require('./format-mapping')

const POSTS_PATH = path.join(__dirname, 'out', '9l-export.json')
const posts = require(POSTS_PATH)

const filename = 'data.json'
const DATA_PATH = path.join(__dirname, 'out', filename)
const PUB_PATH = path.join(__dirname, '../', 'client', 'public', 'assets', filename)
const data$ = fs.createWriteStream(DATA_PATH)

const cleanUpString = (str) => {
  const string = str.toLowerCase()
    .replace(/[`~!@#$%^&*()_|+\-=?;:'',.<>{}[\]\\/]/gi, '')
    .replace(/ /g, '')
    .replace(/é/g, 'e')
    .replace(/ë/g, 'e')
    .replace(/13/g, 'dertiende')
    .replace(/14/g, 'veertien')
  return string
}

const parseMessage = (post) => {
  const { message } = post
  const record = {}
  const attrArray = message.split('\n')
  for (const line of attrArray) {
    if (line.indexOf(':') > -1) {
      const [key, value] = line.split(':')
      record[cleanUpString(key)] = value.trim()
    }
  }
  return record
}

const printResults = () => {
  const dataposts = posts.filter(x => x.dataPost)
  console.log(`#dataposts:  ${dataposts.length}`)
  let dataArray = dataposts.map(x => {
    const cleanedData = {}
    const parsedMessage = parseMessage(x)
    Object.entries(formatMapping).forEach(([key, { prop, sanitization, fallback }]) => {
      cleanedData[key] = parsedMessage[prop] != null
        ? sanitization != null
          ? sanitization(parsedMessage[prop])
          : parsedMessage[prop]
        : fallback
    })
    cleanedData.originalPost = x.message
    cleanedData.date = x.date
    cleanedData.id = x.id
    return cleanedData
  })

  dataArray.splice(0, 1)
  data$.write(JSON.stringify(dataArray, null, 2))
  data$.end()
  data$.on('finish', () => {
    console.log('Wrote all data to file')
    fs.copyFile(DATA_PATH, PUB_PATH, () => {
      console.log('Copied data to public.')
    })
  })
}

printResults()
