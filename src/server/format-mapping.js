const formatMapping = {
  leeftijd: {
    prop: 'leeftijd',
    sanitization: (val) => parseInt(val)
  },
  opleiding: {
    prop: 'opleiding'
  },
  opleidingsNiveau: {
    prop: 'opleiding',
    sanitization: (val) => {
      return (
        val.toLowerCase().includes('phd') || val.toLowerCase().includes('dr.')
          ? 'PhD'
          : val.toLowerCase().includes('master') || val.toLowerCase().includes('ingenieur') ||
              val.toLowerCase().includes('ir. ') || val.toLowerCase().includes('bioir') ||
              val.toLowerCase().includes(' ir') || val.toLowerCase().includes('tew') ||
              val.toLowerCase().includes('ir ') || val.toLowerCase().includes('TEW')
            ? 'Master'
            : val.toLowerCase().includes('bachelor') || val.toLowerCase().includes('a1')
              ? 'Bachelor'
              : val.toLowerCase().includes('secundair') || val.toLowerCase().includes('a2') || val.toLowerCase().includes('tso')
                ? 'Secundair'
                : 'Ander'
      )
    }
  },
  ervaring: {
    prop: 'werkervaringwathoelang'
  },
  burgerlijkeStand: {
    prop: 'alleenstaandgetrouwdwettelijksamenwonend'
  },
  aantalKinderenTenLaste: {
    prop: 'aantalkinderentenlaste'
  },
  statuut: {
    prop: 'arbeiderbediendeambtenaar'
  },
  werkgeverIndustrie: {
    prop: 'werkgeverindustriewaarjeinwerktparitaircomite'
  },
  huidigeFunctie: {
    prop: 'huidigefunctie'
  },
  beschrijving: {
    prop: 'functiebeschrijving'
  },
  ancienniteit: {
    prop: 'ancienniteit'
  },
  arbeidsRegime: {
    prop: 'werkweekparttimefulltimehoeveelurenweekeffectief'
  },
  shift: {
    prop: 'ploegarbeidshiftendagwerk'
  },
  land: {
    prop: 'landwaarjewerkzaambent'
  },
  plaats: {
    prop: 'plaatswaarjewerkzaambent'
  },
  regio: {
    prop: 'plaatswaarjewerkzaambent',
    sanitization: (val) => {
      const value = val.substr('(Stad/Provincie)'.length).toLowerCase().trim()
      if (value.includes('oost-vlaanderen')) {
        return 'Oost-Vlaanderen'
      }
      if (value.includes('gent')) {
        return 'Oost-Vlaanderen'
      }
      if (value.includes('west-')) {
        return 'West-Vlaanderen'
      }
      if (value.includes('bxl')) {
        return 'Brussel'
      }
      if (value.includes('hasselt')) {
        return 'Limburg'
      }
      if (value.includes('antwerpen')) {
        return 'Antwerpen'
      }
      if (value.includes('oslo')) {
        return 'Oslo, Noorwegen'
      }
      return value.length > 0 ? value : 'N/A'
    },
    fallback: 'N/A'
  },
  verantwoordelijkheidPersoneel: {
    prop: 'verantwoordelijkheidoverpersoneel'
  },
  salarisBruto: {
    prop: 'maandsalarisbruto',
    sanitization: (val) => {
      let string = val.toLowerCase()
        .replace(/,/g, '.')
        .replace(/eur/g, '')
        .replace(/€/g, '')
      return parseFloat(string) <= 10.0
        ? parseFloat(string) * 1000
        : parseFloat(string)
    }
  },
  salarisNetto: {
    prop: 'maandsalarisnetto'
  },
  salarisNettoPlusVergoeding: {
    prop: 'maandsalarisnettoinclnettovergoedingen'
  },
  nettovergoedingen: {
    prop: 'nettovergoedingen'
  },
  dertiendeMaand: {
    prop: 'dertiendeemaandvollediggedeeltelijk'
  },
  veertiendeMaand: {
    prop: 'veertienemaandofwinstuitkering'
  },
  gsm: {
    prop: 'gsmeigenbijdrage'
  },
  auto: {
    prop: 'autovaa'
  },
  bedrijfswagen: {
    prop: 'autovaa',
    sanitization: (val) => {
      return (
        val.toLowerCase().includes('ja') || val.toLowerCase().includes('audi')
          ? 'Ja'
          : 'Nee'
      )
    },
    fallback: 'Nee'
  },
  laptop: {
    prop: 'laptopeigenbijdrage'
  },
  maaltijdCheques: {
    prop: 'maaltijdchequeseigenbijdrage'
  },
  ecoCheques: {
    prop: 'ecocheques'
  },
  woonWerkVerkeerVergoed: {
    prop: 'woonwerkverkeervergoed'
  },
  tankkaart: {
    prop: 'tankkaart'
  },
  groepsverzekering: {
    prop: 'groepsverzekering'
  },
  hospitalisatieverzekering: {
    prop: 'hospitalisatieverzekering'
  },
  extra: {
    prop: 'extra'
  },
  andereVoordelen: {
    prop: 'anderevoordelen'
  },
  afstandKm: {
    prop: 'afstandwoonwerkinkms'
  },
  afstandTijd: {
    prop: 'afstandwoonwerkintijd'
  },
  vakantieWettelijk: {
    prop: 'aantalwettelijkevakantiedagen'
  },
  vakantieAdv: {
    prop: 'vakantiedagenarbeidsduurvermindering'
  },
  vakantieExtra: {
    prop: 'vakantiedagenandereextra'
  },
  vrijVerlof: {
    prop: 'vrijheidomverloftenemen'
  },
  glijdendeUren: {
    prop: 'glijdendewerkuren'
  },
  oproepbaar: {
    prop: 'oproepbaardoorwerkgever'
  },
  opleidingen: {
    prop: 'mogelijkheidtothetvolgenvanopleidingen'
  },
  stress: {
    prop: 'stress'
  },
  aantalUurBezig: {
    prop: 'aantaluurweekvoorwerkbezigonderweg'
  },
  overuren: {
    prop: 'hoevaakoverurentepresteren'
  },
  autoNodig: {
    prop: 'autonodigvoorhetwerk'
  },
  telewerk: {
    prop: 'telewerk'
  }
}

module.exports = { formatMapping }
