const fetch = require('node-fetch')
const { Parser } = require('htmlparser2')
const fs = require('fs')
const path = require('path')

const pageUrl = i => `https://www.9lives.be/forum/werk-studie/1096149-wat-uw-brutoloon-deel-8-a-${i}.html`

const FILENAME = `9l-export.json`

const post$ = fs.createWriteStream(path.join(__dirname, 'out', FILENAME))
post$.on('finish', () => console.log('Wrote all data to file'))

let post = {}
let posts = []

const parseOpenTag = (name, attrib) => {
  if (name === 'li' && attrib.id && attrib.id.substr(0, 5) === 'post_') {
    post = { depth: 0, message: '', id: attrib.id }
  }
  if (attrib.id && attrib.id.substr(0, 13) === 'post_message_') {
    post.body = { depth: 0 }
  }

  if (name === 'div' && attrib.class && attrib.class === 'bbcode_quote') {
    post.quote = { depth: 0 }
    post.message += ' [quote] '
  }

  if (name === 'div' && attrib.class && attrib.class === 'bbcode_quote') {
    post.quote = { depth: 0 }
    post.message += ' [quote] '
  }

  if (name === 'span' && attrib.class && attrib.class === 'postdate old') {
    post.date = { depth: 0, date: '' }
  }

  if (post.depth >= 0) post.depth++
  if (post.body && post.body.depth >= 0) post.body.depth++
  if (post.quote && post.quote.depth >= 0) post.quote.depth++
  if (post.date && post.date.depth >= 0) post.date.depth++
}

const parseText = (text) => {
  let trimmedText = text.trim()
    .replace(/Budget\?\)/gi, '?)')
  if (trimmedText <= 0) return
  if (post.quote && post.quote.depth >= 0) {
    post.message += trimmedText + ' '
    return
  }
  if (post.date && post.date.depth >= 0) {
    post.date.date += trimmedText + ' '
    return
  }
  if (post.body && post.body.depth >= 0) {
    post.message += trimmedText
    if (post.dataPost && (trimmedText === '<' ||
      trimmedText === '&' ||
      trimmedText[trimmedText.length - 1] === ':' ||
      trimmedText.substr(trimmedText.length - 4, 4) === '(VAA')) {
      post.message += ' '
    } else {
      post.message += '\n'
    }
    if (!post.dataPost) post.dataPost = trimmedText.includes('PERSONALIA')
    return null
  }
}

const parseCloseTag = (name) => {
  if (post.depth >= 0) post.depth--
  if (post.body && post.body.depth >= 0) post.body.depth--
  if (post.quote && post.quote.depth >= 0) post.quote.depth--
  if (post.date && post.date.depth >= 0) post.date.depth--

  if (post.depth === 0) {
    const it = { message: post.message, dataPost: post.dataPost, id: post.id, date: post.date.date }
    posts.push(Object.assign({}, it))
    post = {}
  }
  if (post.body && post.body.depth === 0) {
    // Closing body
  }
  if (post.quote && post.quote.depth === 0) {
    post.message += ' [/quote] '
  }
}

const options = {
  decodeEntities: true,
  recognizeSelfClosing: true
}

const printResults = () => {
  post$.write(JSON.stringify(posts, null, 2))
  post$.end()
}

const main = async () => {
  for (let j = 1; j <= 10; j++) {
    await fetch(pageUrl(j))
      .then(res => {
        console.log('Fetched ', pageUrl(j))
        const parser = new Parser({
          onopentag: parseOpenTag,
          ontext: parseText,
          onclosetag: parseCloseTag,
          onend: () => console.log('END')
        }, options)
        const $pipe = res.body.pipe(parser)
        $pipe.on('finish', () => parser.end())
      })
  }
  setTimeout(() => {
    console.log('Ended. #posts =', posts.length)
    printResults()
  }, 200)
}

main()
